package main

import (
	"fmt"
	"log"
	"net/http"
)

func HomePageHandler(w http.ResponseWriter, r *http.Request) {
	log.Println(r.Host, "connected on /homepage")
	fmt.Fprint(w, "Upload your config using /config path")
}

package main

import (
	"io/ioutil"
	"log"
	"net/http"
)

func ConfigHandler(w http.ResponseWriter, r *http.Request) {
	log.Println(r.Host, "connected on /config")
	reqBody, _ := ioutil.ReadAll(r.Body)
	payload := string(reqBody)
	endpoints, err := Parse(payload)
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusBadRequest)
	}
	RouterConfig = nil
	AddStaticRoutes()
	AddDynamicRoutes(endpoints)
	go func() {
		restart <- true
	}()
}

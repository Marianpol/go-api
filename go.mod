module dyn-api

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.7.0
	sigs.k8s.io/yaml v1.2.0
)

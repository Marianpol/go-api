package main

import (
	"io/ioutil"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

type TestCase struct {
	Input          []byte
	ExpectedErr    error
	ExpectedOutput []Endpoint
}

func TestParser(t *testing.T) {
	os.Setenv("DELIMITER", "===END===")

	cases := map[string]*TestCase{}

	// Read filename of test files
	files, err := ioutil.ReadDir("./testdata/configfiles")

	if err != nil {
		log.Fatal(err)
	}

	// Load testfiles
	for _, file := range files {
		f, err := ioutil.ReadFile("./testdata/configfiles/" + file.Name())
		if err != nil {
			log.Fatal(err)
		}

		// Set input value for test case
		input := f

		cases[file.Name()] = &TestCase{input, nil, []Endpoint(nil)}
	}

	cases["invalid.json.config"].ExpectedOutput = nil
	cases["invalid.json.config"].ExpectedErr = ErrInvalidConfig
	cases["invalid.yaml.config"].ExpectedOutput = nil
	cases["invalid.yaml.config"].ExpectedErr = ErrInvalidConfig
	cases["valid.json.config"].ExpectedOutput = []Endpoint{
		{
			Method:      "GET",
			Path:        "/validjson",
			Status:      200,
			ContentType: "application/json",
			Content: string([]byte(`[
	{
		"name": "Test Subject",
		"age": 18
	},
	{
		"name": "Underage Test Subject",
		"age": 15
	},
	{
		"name": "Old Test Subject",
		"age": 27
	}
]`)),
		},
		{
			Method:      "POST",
			Path:        "/validjson1",
			Status:      201,
			ContentType: "application/json",
			Content: string([]byte(`{
	"v1": {
		"name": "Test Subject",
		"age": 18
	},
	"v2": {
		"name": "Underage Test Subject",
		"age": 15
	},
	"v3": {
		"name": "Old Test Subject",
		"age": 27
	}
}`)),
		},
		{
			Method:      "DELETE",
			Path:        "/validjson2",
			Status:      203,
			ContentType: "application/json",
			Content: string([]byte(`[
	{
		"name": "Test Subject",
		"age": 18
	},
	{
		"name": "Underage Test Subject",
		"age": 15
	},
	{
		"name": "Old Test Subject",
		"age": 27
	}
]`)),
		},
		{
			Method:      "PUT",
			Path:        "/validjson3",
			Status:      402,
			ContentType: "application/json",
			Content: string([]byte(`[
	{
		"name": "Test Subject",
		"age": 18
	},
	{
		"name": "Underage Test Subject",
		"age": 15
	},
	{
		"name": "Old Test Subject",
		"age": 27
	}
]`)),
		},
	}
	cases["valid.yaml.config"].ExpectedOutput = []Endpoint{
		{
			Method:      "GET",
			Path:        "/validyaml",
			Status:      200,
			ContentType: "text/yaml",
			Content: string([]byte(`---
- name: Test Subject
  age: 18
- name: Underage Test Subject
  age: 15
- name: Old Test Subject
  age: 27`)),
		},
	}
	cases["valid.plaintext.config"].ExpectedOutput = []Endpoint{
		{
			Method:      "GET",
			Path:        "/validplaintext",
			Status:      200,
			ContentType: "text/plain",
			Content: string([]byte(`name: Test Subject
	age: 18
----
name: Underage Test Subject
	age: 15
----
name: Old Test Subject
	age: 27
----`)),
		},
	}

	// Get results from Parser
	assert := assert.New(t)

	for _, c := range cases {
		o, err := Parse(string(c.Input))
		assert.Equal(c.ExpectedOutput, o, "unexpected output")
		assert.Equal(c.ExpectedErr, err, "unexpected error")
	}
}

// func TestGetEndpointDefinition(t *testing.T) {
// 	slicedPayload := GetEndpointsStrings(LoadPayload())

// 	type args struct {
// 		endpointLines string
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 		want Endpoint
// 	}{
// 		{
// 			name: "Endpoint #1",
// 			args: args{
// 				endpointLines: slicedPayload[0],
// 			},
// 			want: Endpoint{
// 				Method:      "POST",
// 				Path:        "/foo",
// 				Status:      200,
// 				ContentType: "application/json",
// 				Content:     `{"foo":{"field":"one"}, "bar":{"field": "two"}}`,
// 			},
// 		},
// 		{
// 			name: "Endpoint #2",
// 			args: args{
// 				endpointLines: slicedPayload[1],
// 			},
// 			want: Endpoint{
// 				Method:      "GET",
// 				Path:        "/bar",
// 				Status:      400,
// 				ContentType: "text/yaml",
// 				Content: `apiVersion: apps/v1
// 			 kind: Deployment
// 			 metadata:
// 			   name: nginx-deployment
// 			 spec:
// 			   selector:
// 				 matchLabels:
// 				   app: nginx
// 			   replicas: 2 # tells deployment to run 2 pods matching the template
// 			   template:
// 				 metadata:
// 				   labels:
// 					 - app: nginx`,
// 			},
// 		},
// 		{
// 			name: "Endpoint #3",
// 			args: args{
// 				endpointLines: slicedPayload[2],
// 			},
// 			want: Endpoint{
// 				Method:      "POST",
// 				Path:        "/thirdpath",
// 				Status:      201,
// 				ContentType: "plain/text",
// 				Content: `foo
// 			 bar

// 			 something else`,
// 			},
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := GetEndpointDefinition(tt.args.endpointLines); got != tt.want {
// 				if got.Method != tt.want.Method {
// 					t.Errorf("Different methods = got %v, want %v", got.Method, tt.want.Method)
// 				}
// 				if got.Path != tt.want.Path {
// 					t.Errorf("Different paths = got %v, want %v", got.Path, tt.want.Path)
// 				}
// 				if got.Status != tt.want.Status {
// 					t.Errorf("Different status = got %v, want %v", got.Status, tt.want.Status)
// 				}
// 				if got.ContentType != tt.want.ContentType {
// 					t.Errorf("Different Content-Type = got %v, want %v", got.ContentType, tt.want.ContentType)
// 				}

// 				gotContentLinesNumber := len(strings.Split(got.Content, "\n"))
// 				wantContentLinesNumber := len(strings.Split(tt.want.Content, "\n"))

// 				if gotContentLinesNumber != wantContentLinesNumber {
// 					t.Errorf("Number of content lines differ = got %v, want %v", gotContentLinesNumber, wantContentLinesNumber)
// 				}
// 			}
// 		})
// 	}
// }

// func LoadPayload() string {
// 	payload, err := ioutil.ReadFile("payload.txt")
// 	if err != nil {
// 		log.Fatalln(err)
// 	}
// 	return string(payload)
// }

package main

func AddStaticRoutes() {
	RouterConfig = append(RouterConfig,
		Route{
			method:  "ANY",
			path:    "/",
			handler: HomePageHandler,
		})
	RouterConfig = append(RouterConfig,
		Route{
			method:  "ANY",
			path:    "/config",
			handler: ConfigHandler,
		})
}

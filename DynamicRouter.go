package main

import (
	"net/http"
)

func AddDynamicRoutes(endpoints []Endpoint) {
	for _, e := range endpoints {
		RouterConfig = append(RouterConfig, Route{
			method: e.Method,
			path:   e.Path,
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("Content-Type", e.ContentType)
				w.WriteHeader(e.Status)
				w.Write([]byte(e.Content))
			},
		})
	}
}

#build stage
FROM golang:1.16.5-alpine AS builder
WORKDIR /go/src/app
COPY . .
RUN go build -o /go/bin/app ./.

FROM alpine:latest
COPY --from=builder /go/bin/app /go/bin/
EXPOSE 8080
ENTRYPOINT ["/go/bin/app"]
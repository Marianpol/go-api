package main

import (
	"context"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

var Router *mux.Router

var Server *http.Server

type Route struct {
	method  string
	path    string
	handler func(http.ResponseWriter, *http.Request)
}

var RouterConfig []Route

var restart = make(chan bool, 1)

func Start() {
	AddStaticRoutes()
	for {
		go func() {
			<-restart
			ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
			defer cancel()
			Server.Shutdown(ctx)
		}()
		log.Println("Service running...")
		Router = mux.NewRouter().StrictSlash(true)
		CreateRoutesFromRouterConfig()
		Server = &http.Server{
			Addr:    "0.0.0.0:8080",
			Handler: Router,
		}
		Server.ListenAndServe()
		log.Println("Restarting service...")
	}
}

func CreateRoutesFromRouterConfig() {
	for _, r := range RouterConfig {
		switch r.method {
		case "ANY":
			Router.HandleFunc(r.path, r.handler)
		case "GET":
			Router.HandleFunc(r.path, r.handler).Methods("GET")
		case "POST":
			Router.HandleFunc(r.path, r.handler).Methods("POST")
		case "DELETE":
			Router.HandleFunc(r.path, r.handler).Methods("DELETE")
		case "PATCH":
			Router.HandleFunc(r.path, r.handler).Methods("PATCH")
		case "PUT":
			Router.HandleFunc(r.path, r.handler).Methods("PUT")
		}
		log.Println("created path", r.path, "with method", r.method)
	}
	log.Println("created", len(RouterConfig), "routes")
}

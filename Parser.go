package main

import (
	"encoding/json"
	"errors"
	"os"
	"regexp"
	"strconv"
	"strings"

	"sigs.k8s.io/yaml"
)

var headerRegex = regexp.MustCompile(`.+\s+\/.+\s+[245]\d{2}\s+.+\/.+\s+`)
var fileTerminator = os.Getenv("DELIMITER")

type Endpoint struct {
	Method      string
	Path        string
	Status      int
	ContentType string
	Content     string
}

var ErrInvalidConfig = errors.New("invalid config")

func isJSONString(s string) bool {
	var v json.RawMessage
	err := json.Unmarshal([]byte(s), &v)
	return err == nil
}

func isYAMLString(s string) bool {
	_, err := yaml.YAMLToJSON([]byte(s))
	return err == nil
}

func Parse(payload string) ([]Endpoint, error) {
	endpointString := GetEndpointsStrings(payload)

	if len(endpointString) > 0 {
		var endpoints []Endpoint
		for _, endString := range endpointString {
			eDef, err := GetEndpointDefinition(endString)
			if err != nil {
				return nil, ErrInvalidConfig
			}
			endpoints = append(endpoints, eDef)
		}
		return endpoints, nil
	}

	return nil, ErrInvalidConfig
}

func GetEndpointsStrings(payload string) []string {
	fileTerminator = os.Getenv("DELIMITER")
	if fileTerminator == "" {
		fileTerminator = "===END==="
	}
	headerRegex := regexp.MustCompile(`.+\s+\/.+\s+[245]\d{2}\s+.+\/.+\s+`)
	endpointRegex := regexp.MustCompile(`.+\s+\/.+\s+[245]\d{2}\s+.+\/.+\s+(.+\s+)+?` + fileTerminator)
	endpointsStrings := endpointRegex.FindAllString(payload, -1)

	if len(endpointsStrings) == 0 {
		return []string{}
	}

	for _, end := range endpointsStrings {
		if len(headerRegex.FindAllString(end, -1)) > 1 {
			return []string{}
		}
	}

	return endpointsStrings
}

func GetEndpointDefinition(endpointString string) (Endpoint, error) {
	header := headerRegex.FindString(endpointString)
	headerIndex := headerRegex.FindStringIndex(endpointString)

	lineRegex := regexp.MustCompile(`\S+\s`)
	headerLines := lineRegex.FindAllString(header, -1)

	delimiterIndex := strings.Index(endpointString, fileTerminator)

	status, _ := strconv.Atoi(strings.TrimSpace(headerLines[2]))
	content := endpointString[headerIndex[1]:delimiterIndex]

	endpoint := Endpoint{
		Method:      strings.TrimSpace(headerLines[0]),
		Path:        strings.TrimSpace(headerLines[1]),
		Status:      status,
		ContentType: strings.TrimSpace(headerLines[3]),
		Content:     strings.TrimSpace(content),
	}

	switch endpoint.ContentType {
	case "application/json":
		if !isJSONString(endpoint.Content) {
			return Endpoint{}, ErrInvalidConfig
		}
	case "text/yaml":
		if !isYAMLString(endpoint.Content) {
			return Endpoint{}, ErrInvalidConfig
		}
	}

	return endpoint, nil
}
